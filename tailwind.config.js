const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge:
    {
      enabled: true,
      content: [
        './public/*.html',
        './public/*.js',
      ]
    },
  theme: {
    colors:{
      primary: {
        700:'#0F4C81',
        500:'#658DC6',
        300:'#C1CEFE',
      },
      secondary: {
        700:'#A58D7F',
        500:'#F5B895',
        300:'#F3D5AD',
      },
      black: defaultTheme.colors.black,
      white: defaultTheme.colors.white,
      gray: defaultTheme.colors.gray,
      ...defaultTheme.colors
    },
    extend: {
      fontFamily: {
        sans: [
          'Nunito',
          ...defaultTheme.fontFamily.sans,
        ]
      }
    }
  },
  variants: {},
  plugins: [],
}
