const fs = require('fs-extra');
const sharp = require('sharp');

path = "./public/images/";
outputPath = "./public/build/images/";

fs.ensureDir(outputPath);

(async () => {
  const dir = await fs.promises.opendir(path)
  for await (const entry of dir) {
    let image = await sharp(path+entry.name);
    let metadata = await image.metadata();

    console.log(`${outputPath + entry.name}`.replace(metadata.format,"jpg"));
    await image
      .jpeg({ progressive: true, force: false })
      .toFormat("jpg")
      .toFile(`${outputPath + entry.name}`.replace(metadata.format,"jpg"));
  }
})()